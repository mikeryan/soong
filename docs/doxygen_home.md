# API reference

This is the API reference documentation for Soong, generated from the code using [Doxygen](http://doxygen.nl/).

All components other than `Record` take a keyed configuration array as their single constructor argument. The component interfaces all inherit from [ConfigurableComponent][d086dd58], and at the moment all concrete configurable component classes inherit from [OptionsResolverComponent][f40a8c73] which is based on [Symfony OptionsResolver][ca87104b]. All components using this base class must implement [optionDefinitions()][43def4f7] to define the configuration options they accept.

  [d086dd58]: interface_soong_1_1_contracts_1_1_configuration_1_1_configurable_component.html "ConfigurableComponent"
  [f40a8c73]: class_soong_1_1_configuration_1_1_options_resolver_component.html "OptionsResolverComponent"
  [ca87104b]: https://symfony.com/doc/current/components/options_resolver.html "Symfony OptionsResolver"
  [43def4f7]: class_soong_1_1_configuration_1_1_options_resolver_component.html#aa3271447a235ee4e580066710a6f35f9 "optionDefinitions"

As an ETL framework, the key components of Soong are of course:

- [Extractors][1b68bb10]: Extractors read data from a source data store and via `extract*()` methods produce iterators to deliver one record at a time as a `Record` instance. They accept configuration to determine where and how to access the source data, including filters (see below) to control what records to process on a given invocation. Being able to tell how many source records are available for migration is very helpful, although on occasion there may be data sources where this is impossible (or at least very slow) - therefore, countability is not required by `Extractor`. Most extractors will want to implement `\Countable` (a `CountableExtractorBase` class is provided which should be a good starting point for most extractors).
- Transformers: A [`RecordTransformer`][6ebf63d0] class accepts a source `Record` and a (possibly partially populated) result `Record` and produces a transformed `Record`. A [`PropertyTransformer`][742cc146] class accepts a value (usually a property from an extractor-produced record) and produces a new value.
- [Loaders][d4c501b1]: Loaders accept one `Record` instance at a time and load the data it contains into a destination as configured. Note that not all destinations may permit deleting loaded data (e.g., a loader could be used to output a CSV file). The deletion capability (used by rollback operations) should be moved to a separate interface.

  [1b68bb10]: interface_soong_1_1_contracts_1_1_extractor_1_1_extractor.html "Extractor"
  [d4c501b1]: interface_soong_1_1_contracts_1_1_loader_1_1_loader.html "Loader"
  [742cc146]: interface_soong_1_1_contracts_1_1_transformer_1_1_property_transformer.html "PropertyTransformer"
  [6ebf63d0]: interface_soong_1_1_contracts_1_1_transformer_1_1_record_transformer.html "RecordTransformer"

The ETL pipeline components need to communicate the data they handle with each other - extractor outputs need to pass through a series of transformers and ultimately into a loader. The canonical representation of such data would be an associative array of arbitrarily-typed values, but rather than require a specific representation it is more flexible to abstract the data.

- [Record][ba5fb4bd]: A data record (a set of named values, which could be any type) is represented by `Record`. In the context of an ETL pipeline, an extractor will output a `Record` to input to transformers, and this feeds into a sequence of record transformers to ultimately deliver the final record to the loader.

  [ba5fb4bd]: interface_soong_1_1_contracts_1_1_data_1_1_record.html "Record"

To manage the migration process, we have:

- [Task][845d1aeb]: A named object controlling the execution of operations according to a set of configuration. Most tasks will be ETL tasks, designed to migrate data, but the overall migration process may require some non-ETL housekeeping tasks (like moving files around) - classes derived from `Task` rather than `EtlTask` can be used to incorporate these operations.
- [EtlTask][fd591c8f]: A Task specifically designed to perform operations on data using extractors, transformers, and loaders. The most important operation is `migrate`, which will:
    1. Invoke an `Extractor` instance and iterate over its data set, retrieving one source `Record` at a time.
    2. Create a destination `Record`, and execute one or more `RecordTransformer` instances to derive the destination record from source properties and configuration.
    3. Pass the destination `Record` to a `Loader` instance for final disposition.
- [TaskContainer][ec470e98]: Manages a list of Tasks.

  [845d1aeb]: interface_soong_1_1_contracts_1_1_task_1_1_task.html "Task"
  [fd591c8f]: interface_soong_1_1_contracts_1_1_task_1_1_etl_task.html "EtlTask"
  [ec470e98]: interface_soong_1_1_contracts_1_1_task_1_1_task_container.html "TaskContainer"

Finally, we have:

- [KeyMap][8129d923]: Storage of the relationships between extracted and loaded records (based on the designated unique keys for each). This enables maintaining relationships between keyed records when the keys change during migration (as when loading into an auto-increment SQL table), as well as providing rollback and auditing capabilities. This component is optional - you may implement ETL processes without tracking the keys being processed.
- [Filter][1ea4455f]: A filter simply accepts a `Record` and based on the record's property values and its own configuration, decides whether the record should be further processed. Filters may be configured in the base configuration of an extractor (to help define the canonical source data to be migrated), or injected at run time (to, say, process a single specific record for debugging).

  [8129d923]: interface_soong_1_1_contracts_1_1_key_map_1_1_key_map.html "KeyMap"
  [1ea4455f]: interface_soong_1_1_contracts_1_1_filter_1_1_filter.html "Filter"
