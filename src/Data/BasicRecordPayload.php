<?php
declare(strict_types=1);

namespace Soong\Data;

use Soong\Contracts\Data\Record;
use Soong\Contracts\Data\RecordPayload;

/**
 * Basic implementation of data records as arrays.
 */
class BasicRecordPayload implements RecordPayload
{

    /**
     * @var \Soong\Contracts\Data\Record
     */
    protected $sourceRecord;

    /**
     * @var \Soong\Contracts\Data\Record
     */
    protected $destinationRecord;

    /**
     * BasicRecordPayload constructor.
     * @param Record $sourceRecord
     * @param Record $destinationRecord
     */
    public function __construct(Record $sourceRecord, Record $destinationRecord)
    {
        $this->sourceRecord = $sourceRecord;
        $this->destinationRecord = $destinationRecord;
    }

    /**
     * {@inheritDoc}
     */
    public function getSourceRecord(): Record
    {
        return $this->sourceRecord;
    }

    /**
     * {@inheritDoc}
     */
    public function getDestinationRecord(): Record
    {
        return $this->destinationRecord;
    }

    /**
     * {@inheritDoc}
     */
    public function setDestinationRecord(Record $record): void
    {
        $this->destinationRecord = $record;
    }
}
