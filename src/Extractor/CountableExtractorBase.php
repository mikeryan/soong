<?php
declare(strict_types=1);

namespace Soong\Extractor;

/**
 * Provides default implementation of count().
 */
abstract class CountableExtractorBase extends ExtractorBase implements \Countable
{

    /**
     * @inheritdoc
     */
    protected function optionDefinitions(): array
    {
        $options = parent::optionDefinitions();
        $options['cache_count'] = [
            'required' => true,
            'default_value' => false,
            'allowed_types' => 'boolean',
        ];
        $options['cache'] = [
            'required' => false,
            'allowed_types' => 'Psr\SimpleCache\CacheInterface',
        ];
        $options['cache_key'] = [
            'required' => false,
            'allowed_types' => 'string',
        ];
        $options['cache_ttl'] = [
            'required' => false,
            'default_value' => 3600,
            'allowed_types' => 'int',
        ];
        return $options;
    }

    /**
     * @inheritdoc
     */
    public function count()
    {
        if (!$this->getConfigurationValue('cache_count')) {
            return $this->getUncachedCount();
        }
        /** @var \Psr\SimpleCache\CacheInterface $cache */
        $cache = $this->getConfigurationValue('cache');
        $cacheKey = $this->getConfigurationValue('cache_key');
        $count = $cache->get($cacheKey);
        if (!isset($count)) {
            $count = $this->getUncachedCount();
            $cache->set($cacheKey, $count, $this->getConfigurationValue('cache_ttl'));
        }
        return $count;
    }

    /**
     * Determine the "real" count directly from the source. Override for
     * sources with more efficient ways to get the count.
     */
    protected function getUncachedCount()
    {
        // By default, use brute force.
        $count = 0;
        foreach ($this->extractAll() as $record) {
            $count++;
        }
        return $count;
    }
}
