<?php
declare(strict_types=1);

namespace Soong\Task;

use Soong\Contracts\Extractor\Extractor;
use Soong\Contracts\KeyMap\KeyMap;
use Soong\Contracts\Loader\Loader;
use Soong\Contracts\Task\EtlTask;

/**
 * Implementation of operations for a full ETL process.
 */
class SimpleEtlTask extends SimpleTask implements EtlTask
{

    /**
     * @inheritdoc
     */
    protected function optionDefinitions(): array
    {
        $options = parent::optionDefinitions();
        $options['extract'] = [
            'required' => true,
            'allowed_types' => 'Soong\Contracts\Extractor\Extractor',
        ];
        $options['transform'] = [
            'allowed_types' => 'Soong\Contracts\Transformer\RecordTransformer[]',
        ];
        $options['load'] = [
            'required' => true,
            'allowed_types' => 'Soong\Contracts\Loader\Loader',
        ];
        $options['key_map'] = [
            'allowed_types' => 'Soong\Contracts\KeyMap\KeyMap',
        ];
        $options['record_factory'] = [
            'required' => true,
            'allowed_types' => 'Soong\Contracts\Data\RecordFactory',
        ];
        return $options;
    }

    /**
     * @inheritdoc
     */
    public function getExtractor(): ?Extractor
    {
        return $this->getConfigurationValue('extract');
    }

    /**
     * @inheritdoc
     */
    public function getLoader(): ?Loader
    {
        return $this->getConfigurationValue('load');
    }

    /**
     * @inheritdoc
     */
    public function getKeyMap(): ?KeyMap
    {
        return $this->getConfigurationValue('key_map');
    }
}
