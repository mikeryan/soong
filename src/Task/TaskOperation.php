<?php
declare(strict_types=1);

namespace Soong\Task;

use Soong\Contracts\Task\Operation;
use Soong\Contracts\Task\Task;

abstract class TaskOperation implements Operation
{

    /**
     * @var \Soong\Contracts\Task\Task
     */
    protected $task;

    public function __construct(Task $task)
    {
        $this->task = $task;
    }
}
