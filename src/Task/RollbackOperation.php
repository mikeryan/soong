<?php
declare(strict_types=1);

namespace Soong\Task;

class RollbackOperation extends TaskOperation
{

    /**
     * @inheritDoc
     */
    public function __invoke(TaskPayload $payload): TaskPayload
    {
        $task = $this->task;
        $processedCount = 0;
        $limit = $payload->getOptions()['limit'] ?? 0;
        if (empty($task->getKeyMap())) {
            return $payload;
        }

        foreach ($task->getKeyMap()->iterate() as $extractedKey) {
            $loadedKey = $task->getKeyMap()->lookupLoadedKey($extractedKey);
            if (!empty($loadedKey)) {
                $task->getLoader()->delete($loadedKey);
                $task->getKeyMap()->delete($extractedKey);
                $processedCount++;
                if (($limit > 0) && $processedCount >= $limit) {
                    return $payload;
                }
            }
        }

        return $payload;
    }
}
