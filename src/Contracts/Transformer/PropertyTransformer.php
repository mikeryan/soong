<?php
declare(strict_types=1);

namespace Soong\Contracts\Transformer;

use Soong\Contracts\Configuration\ConfigurableComponent;

/**
 * Accept a data property and turn it into another data property.
 */
interface PropertyTransformer extends ConfigurableComponent
{
    /**
     * Process the payload. Note this is borrowed from League/Pipeline - we do
     * not use external interfaces in our contracts.
     *
     * @param mixed $payload
     *
     * @return mixed
     */
    public function __invoke($payload);
}
