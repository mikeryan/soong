<?php
declare(strict_types=1);

namespace Soong\Transformer\Property;

use Soong\Configuration\OptionsResolverComponent;
use Soong\Contracts\Task\TaskContainer;
use Soong\Contracts\Transformer\PropertyTransformer;

/**
 * Define common options for most property transformers.
 */
abstract class PropertyTransformerBase extends OptionsResolverComponent implements PropertyTransformer
{

    /**
     * @inheritdoc
     */
    protected function optionDefinitions(): array
    {
        $options = parent::optionDefinitions();
        $options['container'] = [
            'required' => true,
            'allowed_types' => TaskContainer::class,
        ];
        return $options;
    }
}
