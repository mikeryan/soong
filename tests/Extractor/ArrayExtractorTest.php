<?php

namespace Soong\Tests\Extractor;

use Soong\Data\BasicRecordFactory;
use Soong\Tests\Contracts\Extractor\CountableExtractorTestBase;
use Soong\Tests\Contracts\Extractor\TestFilter;

/**
 * Tests the \Soong\Extractor\ArrayExtractor class.
 */
class ArrayExtractorTest extends CountableExtractorTestBase
{

    /**
     * Specify the class we're testing.
     */
    protected function setUp() : void
    {
        $this->extractorClass = '\Soong\Extractor\ArrayExtractor';
    }

    /**
     * Basic test data.
     *
     * @return array
     */
    private function data() : array
    {
        return [
            0 => [
                'positive integer' => 11,
                'negative integer' => -23,
                'numeric string' => '563',
                'zero' => 0,
                'empty string' => '',
                'non-empty string' => 'a string',
                'float' => 1.2345,
                'null' => null,
                'empty array' => [],
                'non-empty array' => [1, 2, 'five'],
                'keyed array' => ['a' => 'b', 'c' => 'd'],
                'object' => new \stdClass(),
                'true' => true,
                'false' => false,
            ],
            1 => [
                'positive integer' => 15,
                'negative integer' => -1,
                'numeric string' => '4',
                'zero' => 0,
                'empty string' => '',
                'non-empty string' => 'a string',
                'float' => 6.78,
                'null' => null,
                'empty array' => [],
                'non-empty array' => ['a', 'b'],
                'keyed array' => ['foo' => 'bar'],
                'object' => new \stdClass(),
                'true' => true,
                'false' => false,
            ],
            2 => [
                'positive integer' => 1235,
                'negative integer' => -2457,
                'numeric string' => '563',
                'zero' => 0,
                'empty string' => '',
                'non-empty string' => 'another string',
                'float' => -0.0001,
                'null' => null,
                'empty array' => [],
                'non-empty array' => ['boo'],
                'keyed array' => [5 => 6],
                'object' => new \stdClass(),
                'true' => true,
                'false' => false,
            ],
        ];
    }

    /**
     * Key property configuration.
     *
     * @return array
     */
    private function keyProperties() : array
    {
        return [
            'positive integer' => [
                'type' => 'integer',
            ]
        ];
    }

    /**
     * Basic extractor configuration for testing.
     *
     * @return array
     */
    private function configuration() : array
    {
        return [
            // @todo Replace with mock.
            'record_factory' => new BasicRecordFactory(),
            'key_properties' => $this->keyProperties(),
            'data' => $this->data(),
        ];
    }

    /**
     * Test extraction of various types of values
     *
     * @return array
     */
    public function extractAllDataProvider() : array
    {
        return [
            'set one' => [$this->configuration(), $this->data()],
        ];
    }

    /**
     * Test filtered extraction of various types of values
     *
     * @return array
     */
    public function extractFilteredDataProvider() : array
    {
        $baseConfiguration = $this->configuration();
        $baseData = $this->data();

        $data['no filters'] = [$baseConfiguration, $this->data()];
        $data['simple filter, no results'] = [
            $baseConfiguration + ['filters' => [
                new TestFilter(['criteria' => ['positive integer', 16]]),
            ]],
            [],
        ];
        $data['simple filter, one results'] = [
            $baseConfiguration + ['filters' => [
                new TestFilter(['criteria' => ['positive integer', 15]]),
            ]],
            [$baseData[1]],
        ];
        $data['simple filter, two results'] = [
            $baseConfiguration + ['filters' => [
                new TestFilter(['criteria' => ['numeric string', '563']]),
            ]],
            [$baseData[0], $baseData[2]],
        ];
        $data['multiple filters, one result'] = [
            $baseConfiguration + ['filters' => [
                new TestFilter(['criteria' => ['numeric string', '563']]),
                new TestFilter(['criteria' => ['non-empty string', 'a string']]),
            ]],
            [$baseData[0]],
        ];

        return $data;
    }

    /**
     * Test retrieval of property metadata.
     *
     * @return array
     */
    public function propertyDataProvider() : array
    {
        $properties = array_keys($this->data()[0]);
        return [
            'set one' => [$this->configuration(), $properties, $this->keyProperties()],
        ];
    }
}
