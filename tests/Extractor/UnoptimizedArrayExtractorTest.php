<?php

namespace Soong\Tests\Extractor;

use Soong\Data\BasicRecordFactory;
use Soong\Tests\Contracts\Extractor\TestFilter;

/**
 * Tests the \Soong\Extractor\ArrayExtractor class.
 */
class UnoptimizedArrayExtractorTest extends ArrayExtractorTest
{

    /**
     * Specify the class we're testing.
     */
    protected function setUp() : void
    {
        $this->extractorClass = '\Soong\Tests\Extractor\UnoptimizedArrayExtractor';
    }

}
