<?php

namespace Soong\Tests\Contracts\Transformer;

use PHPUnit\Framework\TestCase;
use Soong\Contracts\Data\Record;
use Soong\Contracts\Transformer\RecordTransformer;
use Soong\Data\BasicRecordPayload;

/**
 * Base class for testing RecordTransformer implementations.
 *
 * To test a record transformer class, extend this class and implement setUp(),
 * assigning the fully-qualified class name to transformerClass:
 *
 * @code
 *     protected function setUp() : void
 *     {
 *         $this->transformerClass = '\Soong\Transformer\Record\Math';
 *     }
 * @endcode
 *
 * And implement a data provider, where each row of data provided contains a
 * configuration array for the transformer, a source record to pass in, a result
 * record (potentially with some already-populated properties), and the expected
 * result:
 *
 * @code
 *     public function transformerDataProvider()
 *     {
 *         return [
 *             'addition' => [
 *                 ['option1' => 'add'],
 *                 new Record(['source1' => 2, 'source2' => 5]),
 *                 new Record(['dest1' => 8]),
 *                 new Record(['dest1' => 8, 'dest2' => 15]),
 *             ],
 *             'multiplication' => [
 *                 ['option1' => 'multiply'],
 *                 new Record(['source1' => 2, 'source2' => 5]),
 *                 new Record(),
 *                 new Record(['dest1' => 1, 'dest2' => 10]),
 *             ],
 *         ];
 *     }
 * @endcode
 */
abstract class RecordTransformerTestBase extends TestCase
{

    /**
     * Fully-qualified name of a RecordTransformer implementation.
     *
     * @var RecordTransformer $transformerClass
     */
    protected $transformerClass;

    /**
     * Test transform().
     *
     * @dataProvider transformerDataProvider
     *
     * @param array $configuration
     *   Transformer configuration.
     * @param Record $source
     *   Source record to be transformed.
     * @param Record $existingResult
     *   Possibly partially populared result record.
     * @param Record $expected
     *   Expected result of the transformation.
     */
    public function testTransformer(array $configuration, Record $source, Record $existingResult, Record $expected)
    {
        /** @var RecordTransformer $transformer */
        $transformer = new $this->transformerClass($configuration);
        // @todo Mock payload
        $result = $transformer(new BasicRecordPayload($source, $existingResult));
        $this->assertEquals($expected, $result->getDestinationRecord(), "{$this->transformerClass} transformed");
    }
}
