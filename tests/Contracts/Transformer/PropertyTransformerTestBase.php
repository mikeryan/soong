<?php

namespace Soong\Tests\Contracts\Transformer;

use PHPUnit\Framework\TestCase;
use Soong\Contracts\Exception\PropertyTransformerException;
use Soong\Contracts\Task\TaskContainer;
use Soong\Contracts\Transformer\PropertyTransformer;

/**
 * Base class for testing PropertyTransformer implementations.
 *
 * To test a property transformer class, extend this class and implement setUp(),
 * assigning the fully-qualified class name to transformerClass:
 *
 * @code
 *     protected function setUp() : void
 *     {
 *         $this->transformerClass = '\Soong\Transformer\Copy';
 *     }
 * @endcode
 *
 * And implement a data provider, where each row of data provided contains a
 * configuration array for the transformer, a source value to pass in, and the
 * expected result:
 *
 * @code
 *     public function transformerDataProvider()
 *     {
 *         return [
 *             'empty configuration' => [[], 'source_value', 'expected_value'],
 *             'option1 set' => [['option1' => 'setting1', 5, 8],
 *         ];
 *     }
 * @endcode
 */
abstract class PropertyTransformerTestBase extends TestCase
{

    /**
     * Fully-qualified name of a PropertyTransformer implementation.
     *
     * @var PropertyTransformer $transformerClass
     */
    protected $transformerClass;

    /**
     * Test transform().
     *
     * @dataProvider transformerDataProvider
     *
     * @param array $configuration
     *   Transformer configuration.
     * @param mixed $source
     *   Source value to be transformed.
     * @param mixed $expected
     *   Expected result of the transformation.
     * @param string|null $expectedExceptionMessage
     *   Expected exception message, if any.
     */
    public function testTransformer(array $configuration, $source, $expected, ?string $expectedExceptionMessage)
    {
        $configuration['container'] = $this->createMock(TaskContainer::class);
        $transformer = new $this->transformerClass($configuration);
        if (!empty($expectedExceptionMessage)) {
            $this->expectException(PropertyTransformerException::class);
            $this->expectExceptionMessage($expectedExceptionMessage);
            $result = $transformer($source);
        } else {
            $result = $transformer($source);
            $this->assertEquals(
                $expected,
                $result,
                "{$this->transformerClass} transformed"
            );
        }
    }
}
