<?php

namespace Soong\Tests\Contracts\Extractor;

use PHPUnit\Framework\TestCase;
use Soong\Contracts\Extractor\Extractor;

/**
 * Base class for testing Extractor implementations.
 *
 * To test an extractor class, extend this class and implement setUp(),
 * assigning the fully-qualified class name to extractorClass:
 *
 * @code
 *     protected function setUp() : void
 *     {
 *         $this->extractorClass = '\Soong\Extractor\Array';
 *     }
 * @endcode
 *
 * And implement a data provider, where each row of data provided contains a
 * configuration array for the extractor, and the expected records to be
 * extracted expressed as an array:
 *
 * @code
 *     public function extractAllDataProvider()
 *     {
 *         $dataSet1 = [
 *             [
 *                 'field1' => 'value1',
 *                 'field2' => 'value2',
 *                  ...
 *             ],
 *             ...
 *         ];
 *         // Put the above data where the extractor will retrieve it from.
 *         $configuration1 = [
 *             'record_factory' => 'Soong\Data\RecordFactory',
 *             'key_properties' => [
 *                 'field1' => ['type' => 'string'],
 *             ],
 *         ];
 *         return [
 *             'set one' => [$configuration1, $dataSet1],
 *             ...
 *         ];
 *     }
 * @endcode
 */
abstract class ExtractorTestBase extends TestCase
{

    /**
     * Fully-qualified name of a Extractor implementation.
     *
     * @var Extractor $extractorClass
     */
    protected $extractorClass;

    /**
     * Test extractAll().
     *
     * @dataProvider extractAllDataProvider
     *
     * @param array $configuration
     *   Extractor configuration.
     * @param array $expected
     *   Expected set of data records returned.
     */
    public function testExtractAll(array $configuration, array $expected)
    {
        $extractor = new $this->extractorClass($configuration);
        reset($expected);
        $count = 0;
        /** @var \Soong\Contracts\Extractor\Extractor $extractor */
        foreach ($extractor->extractAll() as $record) {
            $count++;
            $this->assertEquals(current($expected), $record->toArray());
            next($expected);
        }
        // Validate the expected number of records were processed.
        $this->assertEquals(count($expected), $count);
    }

    /**
     * Test extractFiltered().
     *
     * @dataProvider extractFilteredDataProvider
     *
     * @param array $configuration
     *   Extractor configuration.
     * @param array $expected
     *   Expected set of data records returned.
     */
    public function testExtractFiltered(array $configuration, array $expected)
    {
        $extractor = new $this->extractorClass($configuration);
        reset($expected);
        $count = 0;
        /** @var \Soong\Contracts\Extractor\Extractor $extractor */
        foreach ($extractor->extractFiltered() as $record) {
            $count++;
            /** @var \Soong\Contracts\Data\Record $record */
            $this->assertEquals(current($expected), $record->toArray());
            next($expected);
        }
        // Validate the expected number of records were processed.
        $this->assertEquals(count($expected), $count);
    }

    /**
     * Test getProperties() and getKeyProperties().
     *
     * @dataProvider propertyDataProvider
     *
     * @param array $configuration
     *   Extractor configuration.
     * @param array $expectedProperties
     *   Expected set of property metadata returned.
     * @param array $expectedKeyProperties
     *   Expected set of key property metadata returned.
     */
    public function testGetProperties(array $configuration, array $expectedProperties, array $expectedKeyProperties)
    {
        $extractor = new $this->extractorClass($configuration);
        $this->assertEquals($expectedProperties, $extractor->getProperties());
        $this->assertEquals($expectedKeyProperties, $extractor->getKeyProperties());
    }
}
