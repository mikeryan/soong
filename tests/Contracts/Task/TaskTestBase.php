<?php

namespace Soong\Tests\Contracts\Task;

use PHPUnit\Framework\TestCase;
use Soong\Task\TaskPayload;
use Soong\Contracts\Task\TaskContainer;

/**
 * Base class for testing Task implementations.
 *
 * To test a task class, extend this class and implement setUp(), assigning
 * the fully-qualified class name to taskClass:
 *
 * @code
 *     protected function setUp() : void
 *     {
 *         $this->taskClass = '\Soong\Task\Task';
 *     }
 * @endcode
 */
abstract class TaskTestBase extends TestCase
{

    /**
     * Fully-qualified name of a Task implementation.
     *
     * @var \Soong\Contracts\Task\Task $taskClass
     */
    protected $taskClass;
}
