<?php

namespace Soong\Tests\Contracts\Task;

use PHPUnit\Framework\TestCase;
use Soong\Contracts\Exception\ComponentNotFound;
use Soong\Contracts\Exception\DuplicateTask;
use Soong\Contracts\Task\Task;
use Soong\Contracts\Task\TaskContainer;

/**
 * Base class for testing TaskContainer implementations.
 *
 * To test a container class, extend this class and implement setUp(), assigning
 * the fully-qualified class name to taskContainerClass:
 *
 * @code
 *     protected function setUp() : void
 *     {
 *         $this->taskContainerClass = '\Soong\Task\SimpleTaskContainer';
 *     }
 * @endcode
 */
abstract class TaskContainerTestBase extends TestCase
{

    /**
     * Fully-qualified name of a TaskContainer implementation.
     *
     * @var TaskContainer $taskContainerClass
     */
    protected $taskContainerClass;

    /**
     * Provides task definitions to add/retrieve with containers.
     *
     * @return array
     *   Each data set array contains an array of task definition arrays. Each
     *   task definition array contains:
     *     task_id: ID of the task.
     *     configuration: Array of configuration for the task.
     */
    public function taskContainerDataProvider() : array
    {
        return [
            'single task' => [
                [
                    [
                        'task_id' => 'task1',
                        'configuration' => [
                            'config_key' => 'config_value',
                        ],
                    ],
                ],
            ],
            'multiple tasks' => [
                [
                    [
                        'task_id' => 'task1',
                        'configuration' => [
                            'config_key' => 'config_value',
                        ],
                    ],
                    [
                        'task_id' => 'task2',
                        'configuration' => [
                            'foo' => 'bar',
                            'stuff' => 'more stuff',
                        ],
                    ],
                    [
                        'task_id' => 'task3',
                        'configuration' => [],
                    ],
                ],
            ],
        ];
    }

    /**
     * Test container add/get/getAll.
     *
     * @dataProvider taskContainerDataProvider
     *
     * @param array $taskList
     *   Array of task definitions, each instance of which contains:
     *     task_id: ID of the task.
     *     configuration: Array of configuration for the task.
     */
    public function testTaskContainer(array $taskList)
    {
        /** @var TaskContainer $container */
        $container = new $this->taskContainerClass();
        foreach ($taskList as $taskDefinition) {
            $task = $this->createMock(Task::class);
            $task->method('getAllConfigurationValues')
                ->willReturn($taskDefinition['configuration']);
            /** @var Task $task */
            $container->add($taskDefinition['task_id'], $task);
            // Verify retrieval of the right task.
            /** @var Task $resultTask */
            $resultTask = $container->get($taskDefinition['task_id']);
            $this->assertEquals(
                $taskDefinition['configuration'],
                $resultTask->getAllConfigurationValues()
            );
        }
    }

    /**
     * Test getAllTasks with no tasks.
     */
    public function testEmptyTaskContainer()
    {
        /** @var TaskContainer $container */
        $container = new $this->taskContainerClass();
        $this->assertEquals([], $container->getAll());
    }

    /**
     * Test trying to retrieve a non-existent task ID.
     */
    public function testMissingTask()
    {
        /** @var TaskContainer $container */
        $container = new $this->taskContainerClass();
        $this->expectException(ComponentNotFound::class);
        $this->expectExceptionMessage("Task i_dont_exist not found.");
        $task = $container->get('i_dont_exist');
    }


    /**
     * Test trying to add a task with a duplicate ID.
     */
    public function testDuplicateTasks()
    {
        /** @var TaskContainer $container */
        $container = new $this->taskContainerClass();
        $taskId = 'dupedupe';
        $task1 = $this->createMock(Task::class);
        $task2 = $this->createMock(Task::class);
        $container->add($taskId, $task1);
        $this->expectException(DuplicateTask::class);
        $this->expectExceptionMessage("Task $taskId already exists.");
        $container->add($taskId, $task2);
    }
}
