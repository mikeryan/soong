<?php

namespace Soong\Tests\Task;

use Soong\Tests\Contracts\Task\EtlTaskTestBase;

/**
 * Tests the \Soong\Task\SimpleEtlTask class.
 */
class EtlTaskTest extends EtlTaskTestBase
{

    /**
     * @inheritdoc
     */
    protected function setUp() : void
    {
        parent::setUp();
        $this->taskClass = '\Soong\Task\SimpleEtlTask';
    }

    public function testNYI() : void
    {
        $this->markTestIncomplete('No task tests have been implemented yet.');
    }
}
