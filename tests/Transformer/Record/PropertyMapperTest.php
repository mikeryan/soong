<?php

namespace Soong\Tests\Transformer\Record;

use Soong\Contracts\Transformer\PropertyTransformer;
use Soong\Data\BasicRecord;
use Soong\Tests\Contracts\Transformer\RecordTransformerTestBase;

/**
 * Tests the \Soong\Transformer\Record\PropertyMapper class.
 */
class PropertyMapperTest extends RecordTransformerTestBase
{

    /**
     * Specify the class we're testing.
     */
    protected function setUp(): void
    {
        $this->transformerClass = '\Soong\Transformer\Record\PropertyMapper';
    }

    /**
     * Test copying of various records.
     *
     * @return array
     */
    public function transformerDataProvider(): array
    {
        $copyTransformer = $this->createMock(PropertyTransformer::class);
        $copyTransformer->expects($this->any())
            ->method('__invoke')
            ->will($this->returnArgument(0));
        /* @todo Mock Records */
        return [
            'simple mappings' => [
                [
                    'property_map' => [
                        'result1' => [
                            [
                                'transformer' => $copyTransformer,
                                'source_property' => 'source1',
                            ],
                        ],
                        'result2' => [
                            [
                                'transformer' => $copyTransformer,
                                'source_property' => 'source2',
                            ],
                        ],
                    ],
                ],
                new BasicRecord(
                    ['source1' => 'foo', 'source2' => 'bar']
                ),
                new BasicRecord([]),
                new BasicRecord(
                    ['result1' => 'foo', 'result2' => 'bar']
                ),
            ],
            'overwrite existing result' => [
                [
                    'property_map' => [
                        'result1' => [
                            [
                                'transformer' => $copyTransformer,
                                'source_property' => 'source1',
                            ],
                        ],
                        'result2' => [
                            [
                                'transformer' => $copyTransformer,
                                'source_property' => 'source2',
                            ],
                        ],
                    ],
                ],
                new BasicRecord(
                    ['source1' => 'foo', 'source2' => 'bar']
                ),
                new BasicRecord(
                    ['result1' => 'blah']
                ),
                new BasicRecord(
                    ['result1' => 'foo', 'result2' => 'bar']
                ),
            ],
            'preserve existing result' => [
                [
                    'property_map' => [
                        'result2' => [
                            [
                                'transformer' => $copyTransformer,
                                'source_property' => 'source2',
                            ],
                        ],
                    ],
                ],
                new BasicRecord(
                    ['source2' => 'bar']
                ),
                new BasicRecord(
                    ['result1' => 'blah']
                ),
                new BasicRecord(
                    ['result1' => 'blah', 'result2' => 'bar']
                ),
            ],
        ];
    }
}
