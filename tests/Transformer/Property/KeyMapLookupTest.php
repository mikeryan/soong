<?php

namespace Soong\Tests\Transformer\Property;

use Soong\Contracts\KeyMap\KeyMap;
use Soong\Contracts\Task\EtlTask;
use Soong\Contracts\Task\TaskContainer;
use Soong\Tests\Contracts\Transformer\PropertyTransformerTestBase;

/**
 * Tests the \Soong\Transformer\Property\KeyMapLookup class.
 */
class KeyMapLookupTest extends PropertyTransformerTestBase
{

    /**
     * Specify the class we're testing.
     */
    protected function setUp() : void
    {
        $this->transformerClass = '\Soong\Transformer\Property\KeyMapLookup';
    }

    /**
     * Test looking up various types of values.
     *
     * @return array
     */
    public function transformerDataProvider() : array
    {
        $configuration['key_map']['task_id'] = 'dummy_task';
        // We will mock a key map instance which returns the last value of
        // each row here when passed the middle value. Note the difference
        // between 'mapped to null' and 'unmatched' is that 'mapped to null' is
        // an explicit mapping to null in the keymap, while the 'unmatched'
        // mapping is not added to the keymap at all.
        $providedData = [
            'integer' => [$configuration, 1, 2, null],
            'string' => [$configuration, 'foo', 'bar', null],
            'mapped to null' => [$configuration, 'baz', null, null],
            'unmatched' => [$configuration, 'blah', null, null, true],
            'null input' => [$configuration, null, null, null],
        ];

        return $providedData;
    }

    /**
     * Override TransformerTestBase::testTransformer, since we need more mocks.
     *
     * @dataProvider transformerDataProvider
     *
     * @param array $configuration
     *   Transformer configuration.
     * @param mixed $source
     *   Source value to be transformed.
     * @param mixed $expected
     *   Expected result of the transformation.
     * @param string|null $expectedExceptionMessage
     *   Expected exception message, if any.
     * @param bool $nullExpected
     *   We expect the keymap to return NULL.
     */
    public function testTransformer(array $configuration, $source, $expected, ?string $expectedExceptionMessage, ?bool $nullExpected = false)
    {
        // Mock the components used to retrieve the mapped key.
        $keyMap = $this->createMock(KeyMap::class);
        $keyMap->method('lookupLoadedKey')->willReturn($nullExpected? null: [$expected]);
        $task = $this->createMock(EtlTask::class);
        $task->method('getKeyMap')->willReturn($keyMap);
        $container = $this->createMock(TaskContainer::class);
        $container->method('get')->willReturn($task);
        $configuration['container'] = $container;

        /** @var \Soong\Transformer\Property\KeyMapLookup $transformer */
        $transformer = new $this->transformerClass($configuration);
        $result = $transformer($source);
        $this->assertEquals($expected, $result, "{$this->transformerClass} transformed");
    }
}
